#!/bin/bash
#

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
RESET="\033[0m"
GOOD_MSG="${GREEN}[+]${RESET} "
BAD_MSG="${RED}[-]${RESET} "

CHECKSUM_COMMAND="sha256sum"
CHECKSUM_FILE="$(date +%Y-%b-%d_%H%M).${CHECKSUM_COMMAND}"
CHECKSUM_RESULTS="verified_$(date +%Y-%b-%d_%H%M).results"

function usage {
    cat <<-EOF
    checksum_all.sh [options]

    -c      create checksums of all files starting from here and output to file
    -u      update checksums file only with new files not already hashed
    -v      verify checksums of all files starting from here using a checksums file
    -h      print this message
EOF
}


function create_checksums {
    echo -e "${GOOD_MSG}Running..."
    $(find . -type f -exec $CHECKSUM_COMMAND "{}" + > $CHECKSUM_FILE)
    if [[ $? -eq 0 ]]; then
        echo -e "${GOOD_MSG}Checksums created successfully. Results output to \"${CHECKSUM_FILE}\""
    else
        echo -e "${BAD_MSG}An error occurred while trying to create checksums. Exiting..." 
        exit 1
    fi
}


function update_checksums {
    echo -e "${GOOD_MSG}Updating checksums file for not already present files..."
    if  [[ $# -eq 1 ]]; then
        if [[ ! -f $1 ]]; then
            echo -e "${BAD_MSG}\"${1}\" checksums file does not exist. Exiting..."
            exit 1
        else
            echo -e "${GOOD_MSG}Updating ${1}..."
            for file in $(find . -type f); do
                if [[ $(grep -c $file $1) -eq 0 ]]; then
                    $CHECKSUM_COMMAND $file >> $1
                fi
            done
            echo -e ${GOOD_MSG}Finished updating \"${1}\" checksums file for new files to hash.
        fi
    fi
}


function verify_checksums {
    sha256sum --check --strict --quiet --ignore-missing $1 2> /dev/null > $CHECKSUM_RESULTS
    echo -e "${GOOD_MSG}Checksums verified successfully. Results output to \"${CHECKSUM_RESULTS}\"."
}


function menu {
    while getopts "cu:v:h" opt; do
        case "${opt}" in
            c) create_checksums;;
            u) shift; update_checksums $1;;
            v) shift; verify_checksums $1;;
            h) usage;;
        esac
    done
}

menu "$@"
