rbackup.sh
==========

Simple rsync, GPG and tar script to backup locations specified in argument file, tar.gz them and optionally encrypt them using GPG symmetric encryption.

Usage:
```
Syntax: rbackup.sh [options] [file]

Options:
-h                                Display this message

-c [file]                         Rsync from a file (uses "from_file" rsync option);
                                  must be relative to home directory and separated by newline

-u [tar.gz to update] -f [file]   Update an rsync tar.gz backup from a file

-e [tar.gz to encrypt]            Encrypt a tar.gz with symmetric gpg encryption;
                                  must be just a name, will look for it in ~/backups

-d [tar.gz to decrypt]            Decrypt a tar.gz file; must be just a name,
                                  will look for it in ~/backups

-x [file]                         Archive with tar.gz and encrypt (basically -c and -e)
```

The backup location is automatically created, if not already existing, in /home/$USER/backups.
