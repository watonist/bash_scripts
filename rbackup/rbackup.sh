#!/usr/bin/env bash

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
RESET="\033[0m"
GOOD_MSG="${GREEN}[+]${RESET} "
BAD_MSG="${RED}[-]${RESET} "

DATETIME="$(date +%Y-%b-%d)"
BACKUP_DEST="/home/${USER}/backups"

RSYNC_FILES_FROM=$1
RSYNC_DEST="${BACKUP_DEST}/rsync"
RSYNC_SOURCE="/home/${USER}"

TAR_OPTS="-cpzf"
TAR_DEST="${BACKUP_DEST}/backup_${DATETIME}.tar.gz"
TAR_DEST_GPG_DECRYPT="${BACKUP_DEST}/backup_decrypted_${DATETIME}.tar.gz"
TAR_SOURCE="-C ${RSYNC_DEST} ."
UNTAR_OPTS="-xpzf"
UNTAR_SOURCE=$2
UNTAR_DEST="-C ${RSYNC_DEST}"

function usage {
cat <<-EOF
    Rsync, tar and GPG backup utility.

    Syntax: $0 [options] [files]

    -h                                    display this message
    -c [file]                             rsync from a file (uses "from_file" rsync option); must be relative to home directory and separated by newline
    -u [tar.gz to update] -f [file]       update an rsync tar.gz backup from a file
    -e [tar.gz to encrypt]                encrypt a tar.gz with symmetric gpg encryption; must be just a name, will look for it in ~/backups
    -d [tar.gz to decrypt]                decrypt a tar.gz file; must be just a name, will look for it in ~/backups
    -x [file]                             tar.gz and encrypt (basically -c and -e)
EOF
}


function provision {
    if [ ! -d "${RSYNC_DEST}" ]; then
        echo -e "${BAD_MSG}Backup directory ${RSYNC_DEST} doesn't exist, creating..."
        mkdir -p "${RSYNC_DEST}"
        echo -e "${GOOD_MSG}Done."
    else
        echo -e "${GOOD_MSG}Backup directory ${RSYNC_DEST} exists. Continuing..."
    fi
}


function check_password_file {
    if [ ! -f $1 ]; then
        echo -e "${BAD_MSG}No password file at $1."
        exit 1
    fi
}


function rsync_from_file {
    RSYNC_OPTS="-iahr --files-from=$1 --delete --exclude venv --log-file=${RSYNC_DEST}/rsync_${DATETIME}.log"
    echo -e "${GOOD_MSG}File supplied is $1"
    while IFS='' read -r LINE || [ -n "${LINE}" ]; do
        if [ ! -d ${RSYNC_SOURCE}/${LINE} ] && [ ! -f ${RSYNC_SOURCE}/${LINE} ]; then
            echo -e "${BAD_MSG}${RSYNC_SOURCE}/${LINE} doesn't exist; check the supplied file contents and make sure the directory/file exists relative to your home and try again."
            exit 1
        else
            echo -e "${GOOD_MSG}Supplied files/dirs ${HOME}/${LINE} exist. Continuing..."
        fi
    done < $1
}


function tar_rsync_backup {
    echo -e "${GOOD_MSG}Running Rsync against your specified directories..."
    RSYNC_COMMAND=$(rsync $RSYNC_OPTS $RSYNC_SOURCE $RSYNC_DEST)
    if [ $? -eq 0 ]; then
        echo -e "${GOOD_MSG}Rsync ran successfully."
        if [ -n "${RSYNC_COMMAND}" ]; then
            echo -e "${GOOD_MSG}Rsync found changes. Recreating the tar.gz archive..."
            TAR_COMMAND=$(tar $TAR_OPTS $TAR_DEST $TAR_SOURCE)
            if [ -n ${TAR_COMMAND} ]; then
                echo -e "${GOOD_MSG}Archiving complete. Backup can be found at ${TAR_DEST}"
                rm -rf ${RSYNC_DEST}
                echo -e "${GOOD_MSG}Removed ${RSYNC_DEST}."
            else
                echo -e "${BAD_MSG}Tar appears to have had a problem when running. Exiting..."
                exit 1
            fi
        else
            echo -e "${BAD_MSG}Rsync found no changes. Will not recreate the tar.gz archive because of this. Exiting..."
        fi
    else
        echo -e "${BAD_MSG} Rsync appears to have had a problem when running. Exiting..."
        exit 1
    fi
}


function untar_rsync_backup {
    echo -e "${GOOD_MSG}Will try to update your backup."
    if [ ! -f ${BACKUP_DEST}/${UNTAR_SOURCE} ]; then
        echo -e "${BAD_MSG}No tar.gz archive in ${BACKUP_DEST}"
    else
        UNTAR_COMMAND=$(tar ${UNTAR_OPTS} ${BACKUP_DEST}/${UNTAR_SOURCE} ${UNTAR_DEST})
        if [ -n ${UNTAR_COMMAND} ]; then
            echo -e "${GOOD_MSG}Untar-ing your previous backup; will rsync again and then rearchive."
        fi
    fi
}


function gpg_encrypt_tar {
    if [ -z $1 ]; then
        if [ ! -f ${TAR_DEST} ]; then
            echo -e "${BAD_MSG}No tar.gz archive in ${TAR_DEST}"
        else
            GPG_COMMAND=$(gpg --symmetric --cipher-algo aes256 $TAR_DEST)
            if [ $? = 0 ]; then
                echo -e "${GOOD_MSG}Encrypted tar.gz archive can be found at ${BACKUP_DEST}/$1.gpg"
            else
                echo -e "${BAD_MSG}An issue occured while to trying to use GPG to encrypt tar archive. Exit code is $?"
            fi
        fi
    else
        if [ ! -f ${BACKUP_DEST}/$1 ]; then
            echo -e "${BAD_MSG}No tar.gz archive in ${BACKUP_DEST}"
        else
            GPG_COMMAND=$(gpg --symmetric --cipher-algo aes256 $BACKUP_DEST/$1)
            if [ $? = 0 ]; then
                echo -e "${GOOD_MSG}Encrypted tar.gz archive can be found at ${BACKUP_DEST}/$1.gpg"
            else
                echo -e "${BAD_MSG}An issue occured while to trying to use GPG to encrypt tar archive. Exit code is $?"
            fi
        fi
    fi
}


function gpg_encrypt_tar_unattended {
    if [ ! -f ${TAR_DEST} ]; then
        echo -e "${BAD_MSG}No tar.gz archive in ${TAR_DEST}"
        exit 1
    else
        GPG_COMMAND=$(gpg --batch --passphrase-file $1 --symmetric --cipher-algo aes256 $TAR_DEST)
        if [ $? = 0 ]; then
            echo -e "${GOOD_MSG}Encrypted tar.gz archive can be found at ${BACKUP_DEST}/${TAR_DEST}.gpg"
        else
            echo -e "${BAD_MSG}An issue occured while to trying to use GPG to encrypt tar archive. Exit code is $?"
        fi
    fi
}


function gpg_decrypt_tar {
    if [ ! -f ${BACKUP_DEST}/$1 ]; then
        echo -e "${BAD_MSG}No tar.gz.gpg archive in ${BACKUP_DEST}"
    else
        GPG_COMMAND=$(gpg -d $BACKUP_DEST/$1 > $TAR_DEST_GPG_DECRYPT)
        #if [ -n ${GPG_COMMAND} ]; then
        if [ $? = 0 ]; then
            echo -e "${GOOD_MSG}Decrypted $1 archive; can be found at ${BACKUP_DEST}"
        else
            echo -e "${BAD_MSG}An issue occured while to trying to use GPG to decrypt. Exit code is $?"
        fi
    fi
}

function tar_and_encrypt {
    rsync_from_file $1
    tar_rsync_backup $1
    gpg_encrypt_tar
}


function tar_and_encrypt_unattended {
    check_password_file $2
    rsync_from_file $1
    tar_rsync_backup $1
    gpg_encrypt_tar_unattended $2
}


function menu {
    while getopts "a:c:u:e:d:x:h" opt; do
        case "${opt}" in
            h)
                usage
                ;;
            a)
                shift
                FROM_FILE=$1
                PASSWORD_FILE=$2
                tar_and_encrypt_unattended ${FROM_FILE} ${PASSWORD_FILE}
                ;;
            c)  
                shift
                FROM_FILE=$1
                provision
                rsync_from_file ${FROM_FILE}
                tar_rsync_backup ${FROM_FILE}
                ;;
            u) 
                shift
                TAR_ARCHIVE=$1
                case "$2" in
                    -f)
                        shift 
                        FROM_FILE=$2
                        ;;
                    *)
                        echo -e "${BAD_MSG}No input file for -u/--update. Provide one with -f <file>."
                        exit 1
                        ;;
                    esac
                provision
                untar_rsync_backup ${TAR_ARCHIVE}
                rsync_from_file ${FROM_FILE}
                tar_rsync_backup ${FROM_FILE}
                ;;
            e)
                shift
                TAR_ARCHIVE=$1
                gpg_encrypt_tar ${TAR_ARCHIVE}
                ;;
            d)
                shift
                GPG_TAR_ARCHIVE=$1
                gpg_decrypt_tar ${GPG_TAR_ARCHIVE}
                ;;
            x)
                shift
                FROM_FILE=$1
                tar_and_encrypt ${FROM_FILE}
                ;;
            :) 
                echo -e "${BAD_MSG}-${OPTARG} expects an argument."
                exit 1
                ;;
            *) 
                echo -e "${BAD_MSG}Unknown commandline option."
                exit 1
                ;;
        esac
    done


}

menu "$@"
