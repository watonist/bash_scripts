#!/bin/bash
set -e

USERNAME=$1

if [[ -z $USERNAME ]]; then
    echo "Supply a username to create as the first argument (\$1)."
    exit 1
fi

function create_user() {
    local USERNAME=$1
    useradd -m $USERNAME
    passwd $USERNAME
    usermod -aG sudo $USERNAME
    cp -r dotfiles/* /home/$USERNAME
    xfce4-session-logout --logout
}


create_user $1
