#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
RESET="\033[0m"
GOOD_MSG="${GREEN}[+]${RESET} "
BAD_MSG="${RED}[-]${RESET} "

function nonzero_return {
    RETVAL=$?
    [[ $RETVAL -ne 0 ]] && echo " [errorcode $RETVAL]"
}


function extract {
    # many thanks to
    # https://askubuntu.com/questions/338758/how-to-quickly-extract-all-kinds-of-archived-files-from-command-line
    for i in $@; do
        if [[ -f $i ]]; then
            case $i in
                *.tar.bz2)  tar xjvf $i     ;;
                *.tar.gz)   tar xzvf $i     ;;
                *.bz2)      bunzip2 $i      ;;
                *.gz)       gunzip $i       ;;
                *.tar)      tar xvf $i      ;;
                *.tbz2)     tar xjvf $i     ;;
                *.tgz)      tar xzvf $i     ;;
                *.zip)      unzip $i        ;;
                *.Z)        uncompress $i   ;;
                *.rar)      rar x $i        ;;
                *.jar)      jar -xvf $i     ;;
                *)          echo -e "${BAD_MSG}Cannot extract ${i}";;
            esac
        else
            echo -e "${BAD_MSG}Invalid file type: ${i}"
        fi
    done
}
