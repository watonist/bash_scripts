" Syntax, tabs, linenumbers etc
" match ErrorMsg '\%>80v.\+'
syntax enable
filetype indent on 
set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set fileformat=unix
"au BufNewFile,BufRead *.py
"    \ set tabstop=8 |
"    \ set softtabstop=4 |
"    \ set shiftwidth=4 |
"    \ set textwidth=79 |
"    \ set expandtab |
"    \ set autoindent |
"    \ set fileformat=unix |
"au BufNewFile,BufRead *.js,*.html,*.css
"    \ set tabstop=2 | 
"    \ set softtabstop=2 |
"    \ set shiftwidth=2 |
set laststatus=2
" set showbreak=+++
set number
" set cursorline
set showmatch
set ignorecase
set encoding=utf-8
scriptencoding utf-8

" Colorscheme and highlighting
set background=dark
colo base16-google-dark
highlight LineNr ctermbg=black
let python_highlight_all = 1

" Lightline settings
"if !has('gui_running')
"    set t_Co=256
"endif

let g:lightline = {
    \ 'colorscheme': 'powerline'
    \ }

" Key mapping for navigating in insert mode
imap HH <left>
imap JJ <up>
imap KK <down>
imap LL <right>

" ESC is far away, use 'jk' instead
inoremap jk <esc>

" gt tabn, tg tabp
nnoremap tg gT

" Switch j with k for up down
" noremap j k
" noremap k j

" Bind B to beginning of line, E to end of file; unbind original $ ^
nnoremap B ^
nnoremap E $
nnoremap $ <nop>
noremap ^ <nop>

" Disable arrow keys
nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <Left> <nop>
nnoremap <Right> <nop>

" Swp files
set noswapfile
