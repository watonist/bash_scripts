source ~/.bash.d/.bash_aliases
source ~/.bash.d/.bash_functions

# Disable that Debian annoying as fuck hardware bell
if [ -n "$DISPLAY" ]; then
    xset b off
    set bell-style none
fi

PS1="(\j)"                                      # background jobs
PS1+="\[\e[32m\][\[\e[m\]\[\e[32m\]\u\[\e[m\]"  # username in green
PS1+=" @ \[\e[31m\]\h\[\e[m\] in"               # hostname in red
PS1+=" \[\e[32m\]\w\[\e[m\]\[\e[32m\]]\[\e[m\]" # working directory in green
PS1+="\`nonzero_return\` \\$ "                  # error code, if != 0
export PS1                                      # export all that

# Custom paths
PATH=~/bin:$PATH
