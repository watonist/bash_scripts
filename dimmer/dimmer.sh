#!/bin/bash

DAY_VALUE="1.0"
NIGHT_VALUE="0.5"
SCREEN=$(xrandr | grep -w connected | awk '{print $1}')

if [[ $1 == "nightmode" ]]; then
    xrandr --output $SCREEN --brightness $NIGHT_VALUE
elif [[ $1 == "daymode" ]]; then
    xrandr --output $SCREEN --brightness $DAY_VALUE
else
    echo "Usage: $0 nightmode|daymode"
fi
