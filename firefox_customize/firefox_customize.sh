#!/bin/bash

set -e

if [[ $EUID -eq 0 ]]; then
    echo "Should not run for the root user! Create an un-privilged user to run as."
    exit 1
fi

DOWNLOAD_SEC_ADDONS_DIR=".addons/sec"
DOWNLOAD_PRIV_ADDONS_DIR=".addons/priv"

INSTALL_PRIV_ADDONS_DIR=".addons/install/priv"
INSTALL_SEC_ADDONS_DIR=".addons/install/sec"

FF_VANILLA_PROFILE="${HOME}/.mozilla/firefox/vanilla-profile"
FF_PRIV_PROFILE="${HOME}/.mozilla/firefox/privacy-profile"
FF_PRIV_PROFILE_EXT_DIR="${HOME}/.mozilla/firefox/privacy-profile/extensions"
FF_SEC_PROFILE="${HOME}/.mozilla/firefox/security-profile"
FF_SEC_PROFILE_EXT_DIR="${HOME}/.mozilla/firefox/security-profile/extensions"

mkdir -p $DOWNLOAD_PRIV_ADDONS_DIR
mkdir -p $DOWNLOAD_SEC_ADDONS_DIR
mkdir -p $INSTALL_PRIV_ADDONS_DIR
mkdir -p $INSTALL_SEC_ADDONS_DIR

function usage() {
    echo "This script will provision Firefox with two user profiles: one for privacy and one for webapp security testing"
    echo "Each profile will have it's own addons and settings"
    echo
    echo "Usage: $0 -[h|d|i|p]"
    echo
    echo "Options:"
    echo "-h:   Display this help message"
    echo "-d:   Only download addons"
    echo "-i:   Only install addons"
    echo "-p:   Both download and install addons"
}

function provision_ff_profiles() {
    if [[ ! -d ${FF_PRIV_PROFILE} ]]; then
        firefox -CreateProfile "privacy ${FF_PRIV_PROFILE}"
        cp user.js ${FF_PRIV_PROFILE}/user.js
        mkdir -p "${FF_PRIV_PROFILE}/extensions"
        echo "Created Firefox privacy profile."
    fi

    if [[ ! -d ${FF_SEC_PROFILE} ]]; then
        firefox -CreateProfile "security ${FF_SEC_PROFILE}"
        mkdir -p "${FF_SEC_PROFILE}/extensions"
        echo "Created Firefox security profile."
    fi

    if [[ ! -d ${FF_VANILLA_PROFILE} ]]; then
        firefox -CreateProfile "vanilla ${FF_VANILLA_PROFILE}"
        echo "Created Firefox vanilla profile."
    fi
}


function download_addons() {
    # Prefbar, Wappalyzer, Live HTTP headers, FireBug, Web Developer, User Agent Switcher, Tamper Data
    SEC_ADDONS_URLS=(https://addons.mozilla.org/firefox/downloads/file/734433/prefbar-7.1.1-fx+sm.xpi
                     https://addons.mozilla.org/firefox/downloads/file/886863/wappalyzer-5.4.11-an+fx.xpi
                     https://addons.mozilla.org/firefox/downloads/file/119979/live_http_headers-0.17-fx+sm.xpi
                     https://addons.mozilla.org/firefox/downloads/file/593058/firebug-2.0.19-fx.xpi
                     https://addons.mozilla.org/firefox/downloads/file/773845/web_developer-2.0.1-an+fx.xpi
                     https://addons.mozilla.org/firefox/downloads/file/785722/user_agent_switcher-1.1.2-an+fx.xpi
                     https://addons.mozilla.org/firefox/downloads/file/79565/tamper_data-11.0.1-fx.xpi
                     https://addons.mozilla.org/firefox/downloads/file/851494/hackbar-1.1.7-an+fx.xpi
    )
    
    # HTTPS Everywhere, uBlock, Privacy Badger, NoScript, Disconnect, Self-Destructing Cookies
    PRIV_ADDONS_URLS=(https://www.eff.org/files/https-everywhere-latest.xpi
                      https://addons.mozilla.org/firefox/downloads/file/894392/ublock_origin-1.15.16-an+fx.xpi
                      https://www.eff.org/files/privacy-badger-latest.xpi
                      https://addons.mozilla.org/firefox/downloads/file/894351/noscript_security_suite-10.1.7.3-an+fx.xpi
                      https://addons.mozilla.org/firefox/downloads/file/616329/self_destructing_cookies-0.4.12-an+fx.xpi
    )

    for i in ${SEC_ADDONS_URLS[@]}; do
        wget -P $DOWNLOAD_SEC_ADDONS_DIR $i
    done

    for i in ${PRIV_ADDONS_URLS[@]}; do
        wget -P $DOWNLOAD_PRIV_ADDONS_DIR $i
    done
}


function _get_addon_id() {
    TARGET_ADDON=$1
    INSTALL_DEST=$2
    MANIFEST_JSON_1=$(unzip -qq -p $TARGET_ADDON manifest.json 2>/dev/null | grep \"id\" | awk '{print $2}' | cut -d'"' -f2)
    MANIFEST_JSON_2=$(unzip -qq -p $TARGET_ADDON manifest.json 2>/dev/null | grep \"name\" | awk '{print $2}' | cut -d'"' -f2)
    INSTALL_RDF=$(unzip -qq -p $TARGET_ADDON install.rdf 2>/dev/null | grep "<em:id>" | head -n 1 | cut -d'>' -f2 | cut -d'<' -f1)

    if [[ ! -z $MANIFEST_JSON_1 ]]; then
        cp -r $TARGET_ADDON "${INSTALL_DEST}/${MANIFEST_JSON_1}.xpi"

    elif [[ ! -z $MANIFEST_JSON_2 ]]; then
        cp -r $TARGET_ADDON "${INSTALL_DEST}/${MANIFEST_JSON_2}.xpi"

    else
        mkdir -p "${INSTALL_DEST}/${INSTALL_RDF}"
        unzip -qq $TARGET_ADDON -d "${INSTALL_DEST}/${INSTALL_RDF}"
    fi
}


function install_addons() {
    provision_ff_profiles
    [[ $(ls -A $DOWNLOAD_PRIV_ADDONS_DIR) ]] || echo "No addons in $DOWNLOAD_PRIV_ADDONS_DIR"
    [[ $(ls -A $DOWNLOAD_SEC_ADDONS_DIR) ]] || echo "No addons in $DOWNLOAD_SEC_ADDONS_DIR"

    for i in $DOWNLOAD_PRIV_ADDONS_DIR/*; do
        _get_addon_id $i $INSTALL_PRIV_ADDONS_DIR
    done
    cp -r $INSTALL_PRIV_ADDONS_DIR/* "${FF_PRIV_PROFILE}/extensions" && rm -rf $INSTALL_PRIV_ADDONS_DIR

    for i in $DOWNLOAD_SEC_ADDONS_DIR/*; do
        _get_addon_id $i $INSTALL_SEC_ADDONS_DIR
    done
    cp -r $INSTALL_SEC_ADDONS_DIR/* "${FF_SEC_PROFILE}/extensions" && rm -rf $INSTALL_SEC_ADDONS_DIR
}


function menu() {
    while getopts "hdip" opt; do
        case "${opt}" in
            h) usage;;
            d) download_addons;;
            i) install_addons;;
            p) download_addons install_addons;;
        esac
    done
}

menu "$@"
