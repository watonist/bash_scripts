#!/bin/bash
# Don't forget to rename and remove .sh; cron doesn't like that
# Insert into cron as follows, for example:
#
# PATH="/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin"
# 01 * * * * <path>/auditd_reporter &> /dev/null
#
# NOTES:
# - don't forget to add the PATH;
# - if generating a report of the last hour, it's best to
#   insert this into cron every hour so you constantly check
#   the system for intrustion signs;

AUREPORT_PARAMS=("--avc" "--auth" "--comm" "--event" "--failed"\
                 "--integrity" "--mods" "--login" "--anomaly" "--executable")

CONF_FILES=("/etc/group" "/etc/gshadow" "/etc/shadow"\
            "/etc/passwd" "/etc/security/opasswd")

LOG_DATE=$(date +%Y-%b-%d-%H_%M)
LOG_PATH="/var/log/auditd/reports"

NOW=$(date "+%m/%d/%Y %H:%M:%S")
ONE_WEEK_AGO=$(date -d "7 days ago" "+%m/%d/%Y %H:%M:%S")
LAST_DAY=$(date -d "24 hours ago" "+%m/%d/%Y %H:%M:%S")
LAST_HOUR=$(date -d "1 hour ago" "+%m/%d/%Y %H:%M:%S")

function provision {
    if [ ! -d "${LOG_PATH}" ]; then
        mkdir -p "${LOG_PATH}"
    fi
}


function get_detailed_reports {
    REPORT_FILE="auditd_detailed_report_${LOG_DATE}.log"
    for i in ${AUREPORT_PARAMS[@]}; do
        COMMAND=$(aureport --input-logs --start $LAST_HOUR --end $NOW -i $i)
        echo "$COMMAND" >> "${LOG_PATH}/${REPORT_FILE}"
    done
}


function get_conf_reports {
    REPORT_FILE="auditd_conf_file_report_${LOG_DATE}.log"
    for i in ${CONF_FILES[@]}; do
        COMMAND=$(ausearch --input-logs --start $LAST_HOUR --end $NOW -i -f $i)
        if [ $? -eq 0 ]; then
            echo -e "$i\n${COMMAND}\n" >> "${LOG_PATH}/${REPORT_FILE}"
        else
            echo -e "$i\nNo match.\n" >> "${LOG_PATH}/${REPORT_FILE}"
        fi
    done
}

provision
get_detailed_reports 
get_conf_reports
