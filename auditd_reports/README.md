auditd_reporter.sh
==================

The script assumes that auditd is already running as a service and configured to watch files and commands, as per DISA STIG RedHat 7 recommendations. At least thats how I use it.

The script is fully automated, intended to be ran as a cronjob. Will check the last hour for events regarding the following files:
- /etc/group
- /etc/gshadow
- /etc/shadow
- /etc/passwd
- /etc/security/opasswd

If any action is taken on these files, they will be reflected in the reports.

Will also check for the following types of events:
- authentication attempts
- avc messages
- events
- failed events for processing
- integrity checks
- account modifications
- login information 
- anomaly events (include NIC promisc mode and segfaults)
- executable events

If any event happens, it will be reflected in the reports.

Reports are created in /var/log/auditd/reports. As per DISA STIG RedHat 7, /var/log/auditd should reside on a separate LVM partition.

NOTE: event time interval can be changed from last hour to last day, by changing the command argument on line 47

TODO:
- send email to arbitrary email address when events happen
